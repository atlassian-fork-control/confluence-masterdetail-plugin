package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.pagination.PageRequest;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.api.service.exceptions.ServiceException;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;

@Component
public class ContentRetriever {

    private final CQLSearchService searchService;
    private final ContentEntityManager contentEntityManager;
    private final I18nResolver i18nResolver;
    // BATCH_SIZE must be a factor of MAX_RESULTS
    public static final int MAX_RESULTS = 500;
    private static final int BATCH_SIZE = 500;

    @Autowired
    public ContentRetriever(@ComponentImport CQLSearchService searchService,
                            @ComponentImport @Qualifier("contentEntityManager") ContentEntityManager contentEntityManager,
                            @ComponentImport I18nResolver i18nResolver) {
        this.searchService = searchService;
        this.i18nResolver = i18nResolver;
        this.contentEntityManager = contentEntityManager;
    }

    public ContentRetrieverResult getContentWithMetaData(@Nonnull final String cql,
                                                                           boolean reverseSort,
                                                                           SearchContext searchContext,
                                                                           final DetailsSummaryMacroMetricsEvent.Builder metrics)
            throws MacroExecutionException {
        // We could potentially optimise the CQL search and then the render in the case where the sorting is by modification
        // date, but we'd have to retrieve extra results because not every CQL match will turn into a row in the rendered
        // table. See comment below re: performance and property indexing. And then sigh, sigh deeply with the
        // knowledge that this unsatisfactory world is only an illusion.
        boolean rowsLimited = false;
        final PageRequest initialPageRequest = new SimplePageRequest(0, BATCH_SIZE);

        String fullCql = "(" + cql + ") and macro = details" + buildOrderByClause(reverseSort);

        metrics.contentSearchStart();
        final List<Content> results = Lists.newArrayList();
        try {
            PageResponse<Content> contents = searchService.searchContent(
                    fullCql,
                    searchContext,
                    initialPageRequest);
            results.addAll(contents.getResults());

            // Not much more performant with CQL, but not worse than before - this macro doesn't cope with large result sets
            // and never will until the page properties are indexed. If that ever happens.
            int nextLimit = results.size() + BATCH_SIZE;
            while (contents.hasMore() && nextLimit <= MAX_RESULTS) {
                final PageRequest nextBatchPageRequest = new SimplePageRequest(results.size(), BATCH_SIZE);
                contents = searchService.searchContent(
                        fullCql,
                        searchContext,
                        nextBatchPageRequest);
                results.addAll(contents.getResults());
                nextLimit = results.size() + BATCH_SIZE;
            }

            if (nextLimit >= MAX_RESULTS && contents.hasMore()) {
                rowsLimited = true;
            }

        } catch (ServiceException e) {
            // Should only get here if a bug in the CQLUI is discovered - this wrap and throw just ensures that the user
            // sees a useful message in a lozenge, instead of an inline cryptic report.
            String msg = i18nResolver.getText("detailssummary.error.searchservice.exception", fullCql, e.getMessage());
            throw new MacroExecutionException(msg, e);
        }
        metrics.contentSearchFinish();
        metrics.labelledContentCount(results.size());

        return new ContentRetrieverResult(asContentEntityObjects(results), rowsLimited);
    }

    /**
     * Always sort by lastModified - if sortBy value is a column and multiple rows have the same cell value, those rows
     * should be consistently sorted.
     */
    private String buildOrderByClause(boolean reverseSort) {
        String order = " order by lastModified";  // this is the default - if column-based sort is specified that sort is done post-hoc

        if (!reverseSort) {
            order += " desc"; // default order is modification date descending
        }
        return order;
    }

    // We still need CEOs until we have time to refactor the rest of the macro code to use Content API objects.
    // (doing it as part of the conversion to CQL-based search wasn't necessary)
    private List<ContentEntityObject> asContentEntityObjects(List<Content> results) {
        List<ContentEntityObject> ceos = Lists.newArrayList();
        for (Content result : results) {
            ContentEntityObject ceo = contentEntityManager.getById(result.getId().asLong());
            if (ceo != null) {
                ceos.add(ceo);
            }
        }
        return ceos;
    }
}
