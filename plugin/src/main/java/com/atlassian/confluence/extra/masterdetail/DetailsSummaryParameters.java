package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.rest.ResourceErrorType;
import com.atlassian.confluence.extra.masterdetail.rest.ResourceException;

import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import java.util.List;

import static java.lang.Math.min;

public class DetailsSummaryParameters {
    private int pageSize;
    private int currentPage;
    private boolean countComments;
    private boolean countLikes;
    private String headingsString;
    private String sortBy;
    private boolean reverseSort;
    private List<ContentEntityObject> content;
    private String id;

    public int getPageSize() {
        return pageSize;
    }

    public DetailsSummaryParameters setPageSize(final int pageSize) {
        this.pageSize = min(DetailsSummaryMacro.MAX_PAGESIZE, pageSize);
        return this;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public DetailsSummaryParameters setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
        return this;
    }

    public Boolean isCountComments() {
        return countComments;
    }

    public DetailsSummaryParameters setCountComments(final boolean countComments) {
        this.countComments = countComments;
        return this;
    }

    public boolean isCountLikes() {
        return countLikes;
    }

    public DetailsSummaryParameters setCountLikes(final boolean countLikes) {
        this.countLikes = countLikes;
        return this;
    }

    @Nullable
    public String getHeadingsString() {
        return headingsString;
    }

    public DetailsSummaryParameters setHeadingsString(final @Nullable String headingsString) {
        this.headingsString = headingsString;
        return this;
    }

    public String getSortBy() {
        return sortBy;
    }

    public DetailsSummaryParameters setSortBy(final String sortBy) {
        this.sortBy = sortBy;
        return this;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public DetailsSummaryParameters setReverseSort(final boolean reverseSort) {
        this.reverseSort = reverseSort;
        return this;
    }

    public List<ContentEntityObject> getContent() {
        return content;
    }

    public DetailsSummaryParameters setContent(final List<ContentEntityObject> content) {
        this.content = content;
        return this;
    }

    public String getId() {
        return id;
    }

    public DetailsSummaryParameters setId(final String id) {
        this.id = id;
        return this;
    }

    public int getTotalPages() {
        return (int) Math.ceil((double) content.size() / (double) pageSize);
    }

    public void checkPageBounds() {
        if (currentPage >= getTotalPages() && getTotalPages() != 0) {
            throw new ResourceException("Requested page is outside bounds", Response.Status.BAD_REQUEST, ResourceErrorType.INVALID_INDEX_PAGE, currentPage);
        }
    }
}
