package com.atlassian.confluence.extra.masterdetail.entities;

import java.util.Collections;
import java.util.List;

/**
 * Represents a paginated details summary page.
 * NOTE: Everything put into this class must be escaped and html safe.
 */
public class PaginatedDetailLines {
    private final List<String> renderedHeadings;
    private List<DetailLine> detailLines;
    private final boolean asyncRenderSafe;

    public PaginatedDetailLines(List<String> renderedHeadings, List<DetailLine> detailLines, boolean asyncRenderSafe) {
        this.renderedHeadings = renderedHeadings;
        this.detailLines = detailLines;
        this.asyncRenderSafe = asyncRenderSafe;
    }

    public List<DetailLine> getDetailLines() {
        return detailLines;
    }

    public List<String> getRenderedHeadings() {
        return renderedHeadings;
    }

    public boolean isAsyncRenderSafe() {
        return asyncRenderSafe;
    }

    public static PaginatedDetailLines empty() {
        return new PaginatedDetailLines(Collections.<String>emptyList(), Collections.<DetailLine>emptyList(), true);
    }
}
