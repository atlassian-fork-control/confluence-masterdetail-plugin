package com.atlassian.confluence.extra.masterdetail.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.google.common.base.Ticker;
import org.joda.time.Duration;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static org.joda.time.Duration.millis;

@EventName("confluence.macro.metrics.details-summary")
public class DetailsSummaryMacroMetricsEvent {
    public enum Type {
        MACRO_EXECUTION, REST_RESOURCE, SERVICE_EXECUTION
    }

    private final Type type;
    private final int spaceCount;
    private final String macroOutputType;
    private final int labelledContentCount;
    private final int permittedContentCount;
    private final int totalExtractedDetailsCount;
    private final int totalFetchedEntityBodySize;
    private final int totalFetchedEntityBodyCount;
    private final int summaryTableLineCount;
    private final int summaryTableHeadingCount;
    private final int summaryTableCommentsCount;
    private final int summaryTableLikesCount;
    private final Duration contentSearchDuration;
    private final Duration permissionFilterDuration;
    private final Duration detailsExtractionDuration;
    private final Duration entityBodyFetchDuration;
    private final Duration summaryTableHeaderBuildDuration;
    private final Duration summaryTableBodyBuildDuration;
    private final Duration summaryTableCommentCountDuration;
    private final Duration summaryTableLikeCountDuration;
    private final Duration templateRenderDuration;

    public DetailsSummaryMacroMetricsEvent(
            final Type type,
            final int spaceCount,
            final String macroOutputType,
            final int labelledContentCount,
            final int permittedContentCount,
            final int totalExtractedDetailsCount,
            final int totalFetchedEntityBodySize,
            final int totalFetchedEntityBodyCount,
            final int summaryTableLineCount,
            final int summaryTableHeadingCount,
            final int summaryTableCommentsCount,
            final int summaryTableLikesCount,
            final Duration contentSearchDuration,
            final Duration permissionFilterDuration,
            final Duration detailsExtractionDuration,
            final Duration entityBodyFetchDuration,
            final Duration summaryTableHeaderBuildDuration,
            final Duration summaryTableBodyBuildDuration,
            final Duration summaryTableCommentCountDuration,
            final Duration summaryTableLikeCountDuration,
            final Duration templateRenderDuration) {
        this.type = type;
        this.spaceCount = spaceCount;
        this.macroOutputType = macroOutputType;
        this.labelledContentCount = labelledContentCount;
        this.permittedContentCount = permittedContentCount;
        this.totalExtractedDetailsCount = totalExtractedDetailsCount;
        this.totalFetchedEntityBodySize = totalFetchedEntityBodySize;
        this.totalFetchedEntityBodyCount = totalFetchedEntityBodyCount;
        this.summaryTableLineCount = summaryTableLineCount;
        this.summaryTableHeadingCount = summaryTableHeadingCount;
        this.summaryTableCommentsCount = summaryTableCommentsCount;
        this.summaryTableLikesCount = summaryTableLikesCount;
        this.contentSearchDuration = contentSearchDuration;
        this.permissionFilterDuration = permissionFilterDuration;
        this.detailsExtractionDuration = detailsExtractionDuration;
        this.entityBodyFetchDuration = entityBodyFetchDuration;
        this.summaryTableHeaderBuildDuration = summaryTableHeaderBuildDuration;
        this.summaryTableBodyBuildDuration = summaryTableBodyBuildDuration;
        this.summaryTableCommentCountDuration = summaryTableCommentCountDuration;
        this.summaryTableLikeCountDuration = summaryTableLikeCountDuration;
        this.templateRenderDuration = templateRenderDuration;
    }

    public Type getType() {
        return type;
    }

    public int getSpaceCount() {
        return spaceCount;
    }

    public String getMacroOutputType() {
        return macroOutputType;
    }

    public int getLabelledContentCount() {
        return labelledContentCount;
    }

    public int getPermittedContentCount() {
        return permittedContentCount;
    }

    public int getTotalExtractedDetailsCount() {
        return totalExtractedDetailsCount;
    }

    public int getTotalFetchedEntityBodySize() {
        return totalFetchedEntityBodySize;
    }

    public int getTotalFetchedEntityBodyCount() {
        return totalFetchedEntityBodyCount;
    }

    public int getSummaryTableLineCount() {
        return summaryTableLineCount;
    }

    public int getSummaryTableHeadingCount() {
        return summaryTableHeadingCount;
    }

    public int getSummaryTableCommentsCount() {
        return summaryTableCommentsCount;
    }

    public int getSummaryTableLikesCount() {
        return summaryTableLikesCount;
    }

    public long getContentSearchDurationMillis() {
        return contentSearchDuration.getMillis();
    }

    public long getPermissionFilterDurationMillis() {
        return permissionFilterDuration.getMillis();
    }

    public long getDetailsExtractionDurationMillis() {
        return detailsExtractionDuration.getMillis();
    }

    public long getEntityBodyFetchDurationMillis() {
        return entityBodyFetchDuration.getMillis();
    }

    public long getSummaryTableHeaderBuildDurationMillis() {
        return summaryTableHeaderBuildDuration.getMillis();
    }

    public long getSummaryTableBodyBuildDurationMillis() {
        return summaryTableBodyBuildDuration.getMillis();
    }

    public long getSummaryTableCommentCountDurationMillis() {
        return summaryTableCommentCountDuration.getMillis();
    }

    public long getSummaryTableLikeCountDurationMillis() {
        return summaryTableLikeCountDuration.getMillis();
    }

    public long getTemplateRenderDurationMillis() {
        return templateRenderDuration.getMillis();
    }

    public static Builder builder(Type type) {
        return new Builder(Ticker.systemTicker(), type);
    }

    public static class Builder {
        private final Ticker ticker;
        private final Type type;

        private int spaceCount;
        private String macroOutputType;
        private int labelledContentCount;
        private int permittedContentCount;
        private int totalExtractedDetailsCount;
        private int totalFetchedEntityBodySize;
        private int totalFetchedEntityBodyCount;
        private int summaryTableLineCount;
        private int summaryTableHeadingCount;
        private int summaryTableCommentsCount;
        private int summaryTableLikesCount;

        private final Timer contentSearchTimer = new Timer();
        private final Timer permissionFilterTimer = new Timer();
        private final Timer detailsExtractionTimer = new Timer();
        private final Timer entityBodyFetchTimer = new Timer();
        private final Timer summaryTableHeadingBuildTimer = new Timer();
        private final Timer summaryTableBodyBuildTimer = new Timer();
        private final Timer summaryTableCommentCountTimer = new Timer();
        private final Timer summaryTableLikeCountTimer = new Timer();
        private final Timer templateRenderTimer = new Timer();

        Builder(final Ticker ticker, final Type type) {
            this.ticker = ticker;
            this.type = type;
        }

        public DetailsSummaryMacroMetricsEvent build() {
            return new DetailsSummaryMacroMetricsEvent(
                    type,
                    spaceCount,
                    macroOutputType,
                    labelledContentCount,
                    permittedContentCount,
                    totalExtractedDetailsCount,
                    totalFetchedEntityBodySize,
                    totalFetchedEntityBodyCount,
                    summaryTableLineCount,
                    summaryTableHeadingCount,
                    summaryTableCommentsCount,
                    summaryTableLikesCount,
                    contentSearchTimer.duration(),
                    permissionFilterTimer.duration(),
                    detailsExtractionTimer.duration(),
                    entityBodyFetchTimer.duration(),
                    summaryTableHeadingBuildTimer.duration(),
                    summaryTableBodyBuildTimer.duration(),
                    summaryTableCommentCountTimer.duration(),
                    summaryTableLikeCountTimer.duration(),
                    templateRenderTimer.duration()
            );
        }

        public Builder spaceCount(final int spaceCount) {
            this.spaceCount = spaceCount;
            return this;
        }

        public Builder macroOutputType(final String macroOutputType) {
            this.macroOutputType = macroOutputType;
            return this;
        }

        public Builder labelledContentCount(final int contentCount) {
            this.labelledContentCount = contentCount;
            return this;
        }

        public Builder permittedContentCount(final int permittedContentCount) {
            this.permittedContentCount = permittedContentCount;
            return this;
        }

        public Builder contentSearchStart() {
            contentSearchTimer.start();
            return this;
        }

        public Builder contentSearchFinish() {
            contentSearchTimer.stop();
            return this;
        }

        public void permissionFilterStart() {
            permissionFilterTimer.start();
        }

        public void permissionFilterFinish() {
            permissionFilterTimer.stop();
        }

        public void detailsExtractionStart() {
            detailsExtractionTimer.start();
        }

        public void detailsExtractionFinish(final int extractedDetailsCount) {
            detailsExtractionTimer.stop();
            totalExtractedDetailsCount += extractedDetailsCount;
        }

        public void entityBodyFetchStart() {
            entityBodyFetchTimer.start();
        }

        public void entityBodyFetchFinish(final int entityBodySize) {
            entityBodyFetchTimer.stop();
            totalFetchedEntityBodySize += entityBodySize;
            totalFetchedEntityBodyCount++;
        }

        public void templateRenderStart() {
            templateRenderTimer.start();
        }

        public void templateRenderFinish() {
            templateRenderTimer.stop();
        }

        public void summaryTableBodyBuildStart() {
            summaryTableBodyBuildTimer.start();
        }

        public void summaryTableBodyBuildFinish(final int lineCount) {
            summaryTableBodyBuildTimer.stop();
            summaryTableLineCount = lineCount;
        }

        public void summaryTableHeadersBuildStart() {
            summaryTableHeadingBuildTimer.start();
        }

        public void summaryTableHeadersBuildFinish(final int headerCount) {
            summaryTableHeadingBuildTimer.stop();
            summaryTableHeadingCount = headerCount;
        }

        public void summaryTableCountCommentsStart() {
            summaryTableCommentCountTimer.start();
        }

        public void summaryTableCountCommentsFinish(final int commentsCount) {
            summaryTableCommentCountTimer.stop();
            this.summaryTableCommentsCount = commentsCount;
        }

        public void summaryTableCountLikesStart() {
            summaryTableLikeCountTimer.start();
        }

        public void summaryTableCountLikesFinish(final int likesCount) {
            summaryTableLikeCountTimer.stop();
            this.summaryTableLikesCount = likesCount;
        }

        private class Timer {
            private long startTimeNanos;
            private long cumulativeDurationNanos;

            void start() {
                startTimeNanos = ticker.read();
            }

            void stop() {
                cumulativeDurationNanos += (ticker.read() - startTimeNanos);
            }

            Duration duration() {
                return millis(MILLISECONDS.convert(cumulativeDurationNanos, NANOSECONDS));
            }
        }
    }
}
