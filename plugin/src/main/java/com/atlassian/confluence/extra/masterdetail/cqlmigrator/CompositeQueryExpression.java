package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.List;

import static com.google.common.base.Joiner.on;

/**
 * Stores multiple {@link QueryExpression}s with a single {@link BooleanOperator} joiner.
 */
public class CompositeQueryExpression implements QueryExpression {
    private final BooleanOperator operator;
    private final List<QueryExpression> expressions;

    private CompositeQueryExpression(BooleanOperator operator, List<? extends QueryExpression> expressions) {
        this.operator = operator;
        this.expressions = ImmutableList.copyOf(expressions);
    }

    @Override
    public String toQueryString() {
        if (expressions.isEmpty()) {
            return "";
        }

        if (expressions.size() > 1) {
            return on(operator.toString()).join(Lists.transform(expressions, QueryExpression::toQueryString));
        }

        return expressions.get(0).toQueryString();
    }

    public boolean isEmpty() {
        return expressions.isEmpty();
    }

    public BooleanOperator getOperator() {
        return operator;
    }

    public QueryExpression get(int i) {
        return expressions.get(i);
    }

    public int size() {
        return expressions.size();
    }

    public static enum BooleanOperator {
        AND(" and "),
        OR(" or ");

        private final String str;

        private BooleanOperator(String str) {
            this.str = str;
        }

        @Override
        public String toString() {
            return str;
        }
    }

    public static Builder builder(BooleanOperator operator) {
        return new Builder(operator);
    }

    public static class Builder {
        private BooleanOperator operator;
        private List<QueryExpression> expressions = Lists.newArrayList();

        private Builder(BooleanOperator operator) {
            this.operator = operator;
        }

        public Builder add(QueryExpression expression) {
            if (expression != null && !(expression instanceof EmptyQueryExpression)) {
                this.expressions.add(expression);
            }
            return this;
        }

        // This builder can return other QueryExpression implementations...
        public QueryExpression build() {
            if (expressions.isEmpty())
                return EmptyQueryExpression.EMPTY;

            if (expressions.size() == 1) {
                // There's no point constructing a wrapper composite for a single expression. The QueryExpression
                // interface allows us to interchange simple and composite expressions, and here is a perfect
                // opportunity to leverage that.
                return expressions.get(0);
            }

            return new CompositeQueryExpression(operator, expressions);
        }
    }
}
