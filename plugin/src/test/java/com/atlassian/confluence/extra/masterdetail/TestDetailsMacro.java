package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.pages.Page;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;

@RunWith(MockitoJUnitRunner.class)
public class TestDetailsMacro {
    @Mock(answer = RETURNS_DEEP_STUBS)
    private PageBuilderService pageBuilderService;

    @Mock
    private I18nResolver i18nResolver;

    private DetailsMacro detailsMacro;

    @Before
    public void setUp() throws Exception {
        detailsMacro = new DetailsMacro(pageBuilderService, i18nResolver);
    }

    @Test
    public void detailsTableSurroundedByDiv() throws Exception {
        final String label = "test";

        assertEquals(
                "<div class='plugin-tabmeta-details'><table></table></div>",
                detailsMacro.execute(
                        new HashMap<String, String>() {
                            {
                                put("label", label);
                            }
                        },
                        "<table></table>",
                        new Page().toPageContext()
                )
        );
    }

    @Test
    public void hideDetailsTable() throws Exception {
        assertEquals(
                "",
                detailsMacro.execute(
                        new HashMap<String, String>() {
                            {
                                put("hidden", "true");
                            }
                        },
                        "<table></table>",
                        new Page().toPageContext()
                )
        );
    }
}
