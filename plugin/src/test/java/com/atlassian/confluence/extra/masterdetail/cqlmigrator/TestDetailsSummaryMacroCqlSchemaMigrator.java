package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestDetailsSummaryMacroCqlSchemaMigrator {
    private DetailsSummaryMacroCqlSchemaMigrator migrator;

    @Before
    public void setUp() throws Exception {
        migrator = new DetailsSummaryMacroCqlSchemaMigrator();
    }

    @Test
    public void testMigrateWithCurrentSpace() throws Exception {
        MacroDefinition macro = MacroDefinition.builder("detailssummary")
                .withParameter("label", "foo")
                .build();

        MacroDefinition migrated = migrator.migrate(macro, null);

        assertThat(migrated.getParameter("cql"), is("label = \"foo\" and space = currentSpace()"));
        assertThat(migrated.getSchemaVersion(), is(2));
    }

    @Test
    public void testMigrateWithGivenSpace() throws Exception {
        MacroDefinition macro = MacroDefinition.builder("detailssummary")
                .withParameter("label", "foo")
                .withParameter("spaces", "TST")
                .build();

        MacroDefinition migrated = migrator.migrate(macro, null);

        assertThat(migrated.getParameter("cql"), is("label = \"foo\" and space = \"TST\""));
    }

    @Test
    public void testMigrateWithMultipleGivenSpaces() throws Exception {
        MacroDefinition macro = MacroDefinition.builder("detailssummary")
                .withParameter("label", "foo")
                .withParameter("spaces", "TST1,TST2")
                .build();

        MacroDefinition migrated = migrator.migrate(macro, null);

        assertThat(migrated.getParameter("cql"), is("label = \"foo\" and space in (\"TST1\",\"TST2\")"));
    }

    @Test
    public void testMigrateRespectsAllSpace() throws Exception {
        MacroDefinition macro = MacroDefinition.builder("detailssummary")
                .withParameter("label", "foo")
                .withParameter("spaces", "@all")
                .build();

        MacroDefinition migrated = migrator.migrate(macro, null);

        assertThat(migrated.getParameter("cql"), is("label = \"foo\""));
    }

    @Test
    public void testMigrateRespectsAllSpaceWithOthers() throws Exception {
        MacroDefinition macro = MacroDefinition.builder("detailssummary")
                .withParameter("label", "foo")
                .withParameter("spaces", "@all,ignored")
                .build();

        MacroDefinition migrated = migrator.migrate(macro, null);

        assertThat(migrated.getParameter("cql"), is("label = \"foo\""));
    }

    @Test
    public void testMigrateTrimsParameterStrings() throws Exception {
        // These strings are possible if someone crafts storage format with the opening/closing tag on a new line.
        // Which they shouldn't.
        MacroDefinition macro = MacroDefinition.builder("detailssummary")
                .withParameter("label", "foo   ")
                .withParameter("spaces", "   bar  ")
                .build();

        MacroDefinition migrated = migrator.migrate(macro, null);

        assertThat(migrated.getParameter("cql"), is("label = \"foo\" and space = \"bar\""));
    }
}