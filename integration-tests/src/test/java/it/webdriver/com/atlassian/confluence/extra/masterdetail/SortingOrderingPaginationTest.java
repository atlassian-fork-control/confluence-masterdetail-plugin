package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SortingOrderingPaginationTest extends AbstractPagePropertiesReportTest {

    @Fixture
    final static PageFixture page2Fixture = PageFixture.pageFixture()
            .title("Page 2")
            .space(space)
            .author(user)
            .build();

    @Fixture
    final static PageFixture page3Fixture = PageFixture.pageFixture()
            .title("Page 3")
            .space(space)
            .author(user)
            .build();

    @Fixture
    final static PageFixture page4Fixture = PageFixture.pageFixture()
            .title("Page 4")
            .space(space)
            .author(user)
            .build();

    @Fixture
    final static PageFixture page5Fixture = PageFixture.pageFixture()
            .title("Page 5")
            .space(space)
            .author(user)
            .build();

    @Fixture
    final static PageFixture page6Fixture = PageFixture.pageFixture()
            .title("Page 6")
            .space(space)
            .author(user)
            .build();

    @BeforeClass
    public static void init() {
        AbstractPagePropertiesReportTest.init();
    }

    @Test
    @ResetFixtures({"page2Fixture", "page3Fixture"})
    public void customFirstColumnHeading() {
        final String firstColumnHeading = "Page Titles";
        updatePage(makeDetailsMarkup("Property 1", "bar"), page2Fixture, label);
        updatePage(makeDetailsMarkup("Property 1", "zzz"), page3Fixture, label);

        updateReportPage(makeReportMarkup(label, null, null,
                firstColumnHeading, "Foo", null, null));
        final PagePropertiesReportMacro summary = gotoReportPage(3);
        assertThat(summary.getColumnNames(), contains(firstColumnHeading, "Foo"));
        assertThat(summary.getColumnValues(firstColumnHeading), contains(page3Fixture.get().getTitle(), page2Fixture.get().getTitle(), propertiesPage.getTitle()));
        assertThat(summary.getColumnValues("Foo"), hasItem("Bar"));
    }

    @Test
    @ResetFixtures({"page2Fixture", "page3Fixture"})
    public void summaryWithSortbyParameter() {
        updatePage(makeDetailsMarkup("Property 1", "bar"), page2Fixture, label);
        updatePage(makeDetailsMarkup("Property 1", "zzz"), page3Fixture, label);

        updatePropertyPage(makeDetailsMarkup("Property 1", "foo"));
        updateReportPage(makeReportMarkup(label, null, null, "Title", null, null, "Property 1"));

        final PagePropertiesReportMacro report = gotoReportPage(3);
        assertThat(report.getColumnValues("Title"), contains("Page 2", propertiesPage.getTitle(), "Page 3"));
    }

    @Test
    @ResetFixtures(value = {"page2Fixture", "page3Fixture", "page4Fixture", "page5Fixture", "page6Fixture"})
    public void summaryNaturalOrdering() {
        List<String> orderedStrings = Arrays.asList( "test1", "test1.1", "test1.10", "test2", "test10", "test100");

        updatePropertyPage(makeDetailsMarkup("Property 1", "test1.10"), label);
        updatePage(makeDetailsMarkup("Property 1", "test1"), page2Fixture, label);
        updatePage(makeDetailsMarkup("Property 1", "test2"), page3Fixture, label);
        updatePage(makeDetailsMarkup("Property 1", "test10"), page4Fixture, label);
        updatePage(makeDetailsMarkup("Property 1", "test100"), page5Fixture, label);
        updatePage(makeDetailsMarkup("Property 1", "test1.1"), page6Fixture, label);

        updateReportPage(makeReportMarkup(label, null, null, "Title", null, null, "Property 1"));
        final PagePropertiesReportMacro report = gotoReportPage(6);

        assertThat(report.getColumnValues("Property 1"), is(orderedStrings));
    }

    @Test
    @ResetFixtures(value = {"page2Fixture", "page3Fixture", "page4Fixture", "page5Fixture", "page6Fixture"})
    public void summaryDateOrdering() {
        List<String> orderedDates = Arrays.asList( "0000-12-31", "1999-01-01", "1999-04-04","2000-01-01", "2000-04-04", "9999-01-01");

        updatePropertyPage(makeDetailsMarkup("Date", "1999-04-04"), label);
        updatePage(makeDetailsMarkup("Date", "2000-01-01"), page2Fixture, label);
        updatePage(makeDetailsMarkup("Date", "2000-04-04"), page3Fixture, label);
        updatePage(makeDetailsMarkup("Date", "1999-01-01"), page4Fixture, label);
        updatePage(makeDetailsMarkup("Date", "9999-01-01"), page5Fixture, label);
        updatePage(makeDetailsMarkup("Date", "0000-12-31"), page6Fixture, label);

        updateReportPage(makeReportMarkup(label, null, null, "Title", null, null, "Date"));
        final PagePropertiesReportMacro report = gotoReportPage(6);

        assertThat(report.getColumnValues("Date"), is(orderedDates));
    }

    @Test
    @ResetFixtures({"page2Fixture"})
    public void summaryRendersAllMetadataOfContentWithSameLabelWithHeadingsInOrderSpecifiedByUser() {
        final String property1 = "property1";
        final String value1 = "value1";
        final String property2 = "property2";
        final String value2 = "value2";
        final String property5 = "property5";
        final String value5 = "value5";

        updatePropertyPage(makeDetailsMarkup(property1, value1, property2, value2, property5, value5));

        final String property3 = "property3";
        final String value3 = "value3";
        final String property4 = "property4";
        final String value4 = "value4";
        final String property6 = "property6";
        final String value6 = "value6";

        updatePage(makeDetailsMarkup(property3, value3, property4, value4, property6, value6), page2Fixture, label);

        final String headings = "property6,property4,property2,property5,property3,property1";
        final String content = "<ac:macro ac:name=\"detailssummary\">" +
                "   <ac:parameter ac:name=\"label\">" + label.getLabel() + "</ac:parameter>" +
                "   <ac:parameter ac:name=\"headings\">" + headings + "</ac:parameter>" +
                "</ac:macro>";
        updateReportPage(content);

        final PagePropertiesReportMacro summary = gotoReportPage(2);

        assertThat(summary.getColumnNames(), contains("Title", property6, property4, property2, property5, property3, property1));
        assertThat(summary.getColumnValues(property1), contains(EMPTY, value1));
        assertThat(summary.getColumnValues(property2), contains(EMPTY, value2));
        assertThat(summary.getColumnValues(property3), contains(value3, EMPTY));
        assertThat(summary.getColumnValues(property4), contains(value4, EMPTY));
        assertThat(summary.getColumnValues(property5), contains(EMPTY, value5));
        assertThat(summary.getColumnValues(property6), contains(value6, EMPTY));
    }

    @Test
    @ResetFixtures({"page2Fixture"})
    public void summaryRendersAllMetadataOfContentWithSameLabelWithHeadingsOrderedAlphabetically() {
        final String property1 = "Property1";
        final String value1 = "value1";
        final String property2 = "Property2";
        final String value2 = "value2";
        final String property5 = "Property5";
        final String value5 = "value5";

        updatePropertyPage(makeDetailsMarkup(property1, value1, property2, value2, property5, value5));

        final String property3 = "Property3";
        final String value3 = "value3";
        final String property4 = "Property4";
        final String value4 = "value4";
        final String property6 = "Property6";
        final String value6 = "value6";

        updatePage(makeDetailsMarkup(property3, value3, property4, value4, property6, value6), page2Fixture, label);
        final String content = "<ac:macro ac:name=\"detailssummary\">" +
                "   <ac:parameter ac:name=\"label\">" + label.getLabel() + "</ac:parameter>" +
                "   <ac:parameter ac:name=\"headings\">" + property1 + "," +
                property2 + "," +
                property3 + "," +
                property4 + "," +
                property5 + "," +
                property6 + "</ac:parameter>" +
                "</ac:macro>";
        updateReportPage(content);
        final PagePropertiesReportMacro summary = gotoReportPage(2);

        assertThat(summary.getColumnNames(), contains("Title", property1, property2, property3, property4, property5, property6));
        assertThat(summary.getColumnValues(property1), contains(EMPTY, value1));
        assertThat(summary.getColumnValues(property2), contains(EMPTY, value2));
        assertThat(summary.getColumnValues(property3), contains(value3, EMPTY));
        assertThat(summary.getColumnValues(property4), contains(value4, EMPTY));
        assertThat(summary.getColumnValues(property5), contains(EMPTY, value5));
        assertThat(summary.getColumnValues(property6), contains(value6, EMPTY));
    }
}
